#!/bin/bash

# copia os ficheiros html de cada página para um dir com um index.html
# ex: pages/fazer-a-diferenca.html -> dist/fazer-a-diferenca/index.html
#
# Isto é estúpido e devia estar na Makefile, mas não estou a conseguir
# meter lá o forloop e não tenho tempo pra fazer melhor

cd pages
for f in *.html; do 
  mkdir -p ../dist/${f/\.html/}; 
  cp $f ../dist/${f/\.html/}/index.html; 
done
